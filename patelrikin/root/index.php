<?php 
// set the default timezone to use. Available since PHP 5.1
date_default_timezone_set('America/New_York');
?>
<!DOCTYPE html">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>Rikin Patel</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="Description" content="This is Rikin Patel's work in progress website. Stay tuned!">
    <meta name="keywords" content="patelrikin, rikinpatel, rikin, patel, rik">
	<meta name="author" content="Rikin Patel">
	<meta name="viewport" content="width=device-width, initial-scale=1">

    <link type="text/css" href="/stylesheet/main.css" rel="stylesheet">
    <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap.min.css">

    <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.2.16/angular.min.js"></script>
    <!--[if lt IE 9]>
		<script src="html5shiv.js"></script>
	<![endif]-->
</head>
<body>
	<div class="container">
        <div class="jumbotron">
            <h1><?php echo "Hello Root!" ?></h1>
            <h2><?php echo "Welcome to Rikin Patel's Homepage!" ?></h2>
            <p><?php $today = date("l jS \of F Y h:i:s A \E\S\T"); // Prints something like: Monday 8th of August 2005 03:12:46 PM EST
            print_r($today); ?></p>
        </div>

		<div id="progress-container">
			<div class="progress progress-striped active">
				<div class="progress-bar" role="progressbar" aria-valuenow="5" aria-valuemin="0" aria-valuemax="100" style="width: 5%;">
					<span class="sr-only">5% Complete</span>
				</div>
            </div>
			<span id="html5-progress">
            	<img src="http://www.w3.org/html/logo/badge/html5-badge-h-css3.png" width="133" height="64" alt="HTML5 Powered with CSS3 / Styling" title="HTML5 Powered with CSS3 / Styling">
			</span>
		</div>
        
	</div>

	<p id="seo-keywords">This is Rikin Patel's work in progress website. Stay tuned! patelrikin rikinpatel rik rikin root patel homepage welcome facebook linkedin twitter</p>
	<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
    <script src="//netdna.bootstrapcdn.com/bootstrap/3.1.1/js/bootstrap.min.js"></script>
	<?php include_once("include/alltrackingpixels.php") ?>

</body>
</html>
