<?php
/**
 * The base configurations of the WordPress.
 *
 * This file has the following configurations: MySQL settings, Table Prefix,
 * Secret Keys, WordPress Language, and ABSPATH. You can find more information
 * by visiting {@link http://codex.wordpress.org/Editing_wp-config.php Editing
 * wp-config.php} Codex page. You can get the MySQL settings from your web host.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'jigesh_wp1');

/** MySQL database username */
define('DB_USER', 'jigesh_wp1');

/** MySQL database password */
define('DB_PASSWORD', 'G[EsvdI3N550(~7');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '6g9bE5YyRk1SO0JQySG6ZWFMXUzc85re4Cwi01XFckJaqbu9yOYQ45ABzEI34R1W');
define('SECURE_AUTH_KEY',  '0d0MG5vSE4Q2e8TUcoNmVIBCot5mwi0JVdwjw1mZsKrHCIRnQJqRhCkFu6j2unHw');
define('LOGGED_IN_KEY',    '5bxoUn2SPbrvqpHrKYJ7ecY1Z1RhmLf2FPY1K5he3UgO0ND3CLOStZIs2iVjk4IO');
define('NONCE_KEY',        '7bl594ooivKC6RYHVBbJ0whDsgWX2MPFfVszZn71kkEGhozqFcnZtXJj93e7mpvh');
define('AUTH_SALT',        'suTHw3l9BNHsswOSro0V452nPmyhJoTFIxs4MENms9YFhNzHZKI89HhX4vFwPpcM');
define('SECURE_AUTH_SALT', 'LccRLCNUWvdvSIVp9KsXJFgASwcs3hBtJoEsM8ZrNzauPQSNZMmQnvjoe7ZAv9nf');
define('LOGGED_IN_SALT',   'MSTd7RNOHWCG4IUHTcrbgjsR5gcSIN22UtwK6iPK2gvWIYjxzLiUlgp0OFjgGFvG');
define('NONCE_SALT',       'JG8zUgdscIa4Mvv45ugPEi664AQDNqb8AIsXmAA7hOOngnd3DAH5Gylzgigw0OZx');

/**
 * Other customizations.
 */
define('FS_METHOD','direct');define('FS_CHMOD_DIR',0755);define('FS_CHMOD_FILE',0644);
define('WP_TEMP_DIR',dirname(__FILE__).'/wp-content/uploads');

/**
 * Turn off automatic updates since these are managed upstream.
 */
define('AUTOMATIC_UPDATER_DISABLED', true);


/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each a unique
 * prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * WordPress Localized Language, defaults to English.
 *
 * Change this to localize WordPress. A corresponding MO file for the chosen
 * language must be installed to wp-content/languages. For example, install
 * de_DE.mo to wp-content/languages and set WPLANG to 'de_DE' to enable German
 * language support.
 */
define('WPLANG', '');

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
